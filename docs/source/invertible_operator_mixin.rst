.. currentmodule:: nifty

The ``InvertibleOperatorMixin`` class -- ...
............................................

.. autoclass:: InvertibleOperatorMixin
    :show-inheritance:
    :members:
