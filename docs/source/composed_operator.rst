.. currentmodule:: nifty

The ``ComposedOperator`` class -- A possibility to combine several operators
............................................................................

.. autoclass:: ComposedOperator
    :show-inheritance:
    :members:
