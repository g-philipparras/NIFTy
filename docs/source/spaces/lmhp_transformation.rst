.. currentmodule:: nifty

The ``LMHPTransformation`` class -- A transformation routine
............................................................

.. autoclass:: LMHPTransformation
    :show-inheritance:
    :members:
