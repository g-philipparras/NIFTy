.. currentmodule:: nifty

The ``GLLMTransformation`` class -- A transformation routine
............................................................


.. autoclass:: GLLMTransformation
    :show-inheritance:
    :members:
