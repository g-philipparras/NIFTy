Transformations
---------------
NIFTY helper classes for transformations betwenn NIFTY spaces.

.. toctree:: 
    :maxdepth: 1
    
    rgrg_transformation
    gllm_transformation
    hplm_transformation
    lmgl_transformation
    lmhp_transformation

