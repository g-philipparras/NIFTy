.. currentmodule:: nifty

The ``PowerSpace`` class --  NIFTY class for spaces of power spectra
....................................................................

.. autoclass:: PowerSpace
    :show-inheritance:
    :members:
