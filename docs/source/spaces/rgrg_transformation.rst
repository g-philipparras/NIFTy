.. currentmodule:: nifty

The ``RGRGTransformation`` class -- A transformation routine
............................................................


.. autoclass:: RGRGTransformation
    :show-inheritance:
    :members:
