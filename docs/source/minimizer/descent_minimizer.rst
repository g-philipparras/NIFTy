.. currentmodule:: nifty

The ``DescentMinimizer`` class -- The Base class for minimizers
...............................................................

.. autoclass:: DescentMinimizer
    :show-inheritance:
    :members:
