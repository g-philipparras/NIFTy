.. currentmodule:: nifty

The ``PropagatorOperator`` class -- NIFTY Propagator Operator D
...............................................................

.. autoclass:: PropagatorOperator
    :show-inheritance:
    :members:
