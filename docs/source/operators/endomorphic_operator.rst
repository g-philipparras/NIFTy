.. currentmodule:: nifty

The ``EndomorphicOperator`` class -- ...
........................................

.. autoclass:: EndomorphicOperator
    :show-inheritance:
    :members:
