.. currentmodule:: nifty

The ``InvertibleOperatorMixin`` class -- Mixin class to invert implicit defined operators
.........................................................................................

.. autoclass:: InvertibleOperatorMixin
    :show-inheritance:
    :members:
