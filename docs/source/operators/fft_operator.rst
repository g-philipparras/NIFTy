.. currentmodule:: nifty

The ``FFTOperator`` class -- Transforms between a pair of position and harmonic domains
.......................................................................................

.. autoclass:: FFTOperator
    :show-inheritance:
    :members:
