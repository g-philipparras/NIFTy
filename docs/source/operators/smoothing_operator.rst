.. currentmodule:: nifty

The ``SmoothingOperator`` class -- NIFTY class for smoothing operators
......................................................................

.. autoclass:: SmoothingOperator
    :show-inheritance:
    :members:
