.. currentmodule:: nifty

The ``ProjectionOperator`` class -- ...
.......................................

.. autoclass:: ProjectionOperator
    :show-inheritance:
    :members:
