.. currentmodule:: nifty

The ``DiagonalOperator`` class -- ...
.....................................

.. autoclass:: DiagonalOperator
    :show-inheritance:
    :members:
