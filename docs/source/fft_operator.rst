.. currentmodule:: nifty

The ``FFTOperator`` class -- Fourier Transformations
....................................................

.. autoclass:: FFTOperator
    :show-inheritance:
    :members:
