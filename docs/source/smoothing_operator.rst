.. currentmodule:: nifty

The ``SmoothingOperator`` class -- Smoothing fields
...................................................

.. autoclass:: SmoothingOperator
    :show-inheritance:
    :members:
