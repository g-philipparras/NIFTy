.. currentmodule:: nifty

The ``ResponseOperator`` class -- A possible response implementation
....................................................................

.. autoclass:: ResponseOperator
    :show-inheritance:
    :members:
