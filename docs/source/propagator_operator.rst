.. currentmodule:: nifty

The ``PropagatorOperator`` class -- ...
.......................................

.. autoclass:: PropagatorOperator
    :show-inheritance:
    :members:
