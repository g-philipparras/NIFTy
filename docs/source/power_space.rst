.. currentmodule:: nifty

The ``PowerSpace`` class --  The natural space underlying power-spectra
.......................................................................

.. autoclass:: PowerSpace
    :show-inheritance:
    :members:
